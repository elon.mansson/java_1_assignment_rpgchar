package chars;

import models.Character;
import models.PrimaryAttributes;

import java.util.ArrayList;

public class Rogue extends Character {
    public Rogue(String name) throws Exception {
        super(name, 1, new PrimaryAttributes(1,2,6), "DEX");
        var usableWeapons = new ArrayList<String>();
        usableWeapons.add("DAGGERS");
        usableWeapons.add("SWORDS");
        usableWeapons.add("NULL");
        var usableArmors = new ArrayList<String>();
        usableArmors.add("LEATHER");
        usableArmors.add("MAIL");
        usableArmors.add("NULL");
        super.setUsableArmor(usableArmors);
        super.setUsableWeapons(usableWeapons);
        super.setTotalAttributes();
    }

    @Override
    public void levelUp() throws Exception {
        super.levelUp();
        var currentLevel = super.getLevel();
        if (currentLevel != 1){
            setPrimaryAttributes(new PrimaryAttributes( currentLevel, currentLevel + 1, 4*currentLevel + 2));
        }
        super.setTotalAttributes();
    }

    @Override
    public String toString() {
        return "Rogue" + super.getPrimaryAttribute();
    }
}
