package chars;

import models.Character;
import models.PrimaryAttributes;

import java.util.ArrayList;

public class Ranger extends Character {
    public Ranger(String name) throws Exception {
        super(name, 1, new PrimaryAttributes(1,1,7), "DEX");
        var usableWeapons = new ArrayList<String>();
        usableWeapons.add("BOW");
        usableWeapons.add("NULL");
        var usableArmors = new ArrayList<String>();
        usableArmors.add("LEATHER");
        usableArmors.add("MAIL");
        usableArmors.add("NULL");
        super.setUsableArmor(usableArmors);
        super.setUsableWeapons(usableWeapons);
        super.setTotalAttributes();
    }
    @Override
    public void levelUp() throws Exception {
        super.levelUp();
        var currentLevel = super.getLevel();
        if (currentLevel != 1){
            setPrimaryAttributes(new PrimaryAttributes(currentLevel, currentLevel, 5*currentLevel + 2));
        }
        super.setTotalAttributes();
    }

    @Override
    public String toString() {
        return "Ranger{";
    }
}
