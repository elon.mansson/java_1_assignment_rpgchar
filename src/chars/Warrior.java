package chars;

import models.Character;
import models.PrimaryAttributes;

import java.util.ArrayList;

public class Warrior extends Character {
    public Warrior(String name) throws Exception {
        super(name, 1, new PrimaryAttributes(1,5,2), "STR");
        var usableWeapons = new ArrayList<String>();
        usableWeapons.add("AXES");
        usableWeapons.add("HAMMERS");
        usableWeapons.add("SWORDS");
        usableWeapons.add("NULL");
        var usableArmors = new ArrayList<String>();
        usableArmors.add("MAIL");
        usableArmors.add("PLATE");
        usableArmors.add("NULL");
        super.setUsableArmor(usableArmors);
        super.setUsableWeapons(usableWeapons);
        super.setTotalAttributes();
    }

    @Override
    public void levelUp() throws Exception {
        super.levelUp();
        var currentLevel = super.getLevel();
        if (currentLevel != 1){
            setPrimaryAttributes(new PrimaryAttributes( currentLevel, 3*currentLevel + 2, 2*currentLevel));
        }
        super.setTotalAttributes();
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "Intelligence=" + super.getPrimaryAttribute() +
                ", Strength=" +
                ", Dexterity=" +
                '}';
    }
}
