package chars;

import models.Character;
import models.PrimaryAttributes;
import models.Weapon;

import java.util.ArrayList;

public class Mage extends Character {
    public Mage(String name) throws Exception {
        super(name, 1, new PrimaryAttributes(8,1,1), "INT");
        var usableWeapons = new ArrayList<String>();
        usableWeapons.add("STAFFS");
        usableWeapons.add("WANDS");
        usableWeapons.add("NULL");
        var usableArmors = new ArrayList<String>();
        usableArmors.add("CLOTH");
        usableArmors.add("NULL");
        super.setUsableArmor(usableArmors);
        super.setUsableWeapons(usableWeapons);
        super.setTotalAttributes();
    }

    @Override
    public void levelUp() throws Exception {
        super.levelUp();
        var currentLevel = super.getLevel();
        if (currentLevel != 1){
            setPrimaryAttributes(new PrimaryAttributes( 5*currentLevel + 3, currentLevel, currentLevel));
        }
        super.setTotalAttributes();
    }

    @Override
    public String toString() {
        return "Mage";
    }
}
