package utils;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
