package utils;

public enum Slot {
    WEAPON,
    BODY,
    HEAD,
    LEGS
}
