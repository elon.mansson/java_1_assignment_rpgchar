package models;

import utils.ArmorType;
import utils.Slot;

public class Armor {
    String name;
    int level;
    Slot slot;
    ArmorType armorType;
    PrimaryAttributes primaryAttributes;

    public Armor(String name, int level, Slot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) {
        this.name = name;
        this.level = level;
        this.slot = slot;
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }

    @Override
    public String toString() {
        return "Armor{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", slot=" + slot +
                ", armorType=" + armorType +
                '}';
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public String getName() {
        return name;
    }
}
