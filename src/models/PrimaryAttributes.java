package models;

public class PrimaryAttributes {
    private int Intelligence;
    private int Strength;
    private int Dexterity;

    public PrimaryAttributes(int intelligence, int strength, int dexterity) {
        Intelligence = intelligence;
        Strength = strength;
        Dexterity = dexterity;
    }

    public int getIntelligence() {
        return Intelligence;
    }

    public int getStrength() {
        return Strength;
    }

    public int getDexterity() {
        return Dexterity;
    }


    @Override
    public String toString() {
        return "PrimaryAttributes{" +
                "Intelligence=" + Intelligence +
                ", Strength=" + Strength +
                ", Dexterity=" + Dexterity +
                '}';
    }
}
