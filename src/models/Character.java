package models;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import utils.Slot;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Character {
    private String name;
    private int level;
    private PrimaryAttributes primaryAttributes;
    private PrimaryAttributes totalAttributes;
    private int dexterityFromGear;
    private int strengthFromGear;
    private int intelligenceFromGear;
    private int primaryAttribute;
    private String mainAttribute;
    private double Dps;
    private Weapon weapon;
    private ArrayList<String> usableWeapons;
    private ArrayList<String> usableArmors;
    private HashMap<Slot, Armor> gear = new HashMap<Slot, Armor>();

    public Character(String name, int level, PrimaryAttributes primaryAttributes, String mainAttribute) {
        this.name = name;
        this.level = level;
        this.primaryAttributes = primaryAttributes;
        this.mainAttribute = mainAttribute;
    }

    public double getDps() {
        return Dps;
    }

    public boolean setGear(Armor armor) throws Exception {
        this.dexterityFromGear = 0;
        this.strengthFromGear = 0;
        this.intelligenceFromGear = 0;

        if(armor.level > this.level){
            setTotalAttributes();
            throw new InvalidArmorException("The level requirement is to high!");
        }

        for (String UsableArmor : usableArmors) {
            if(UsableArmor.equals(armor.armorType.toString())) {
                this.gear.put(armor.slot, armor);
                setTotalAttributes();
                return true;
            }
        }
        setTotalAttributes();
        throw new InvalidArmorException("You cant use this type of armor");
    }

    public boolean setDps() throws Exception {
        if(this.weapon == null){
            this.weapon = new Weapon();
        }
        if(this.weapon.level > this.level){
            throw new InvalidWeaponException("The level requirement is to high!");
        }
        for (String usableWeapon : usableWeapons) {
            if (usableWeapon == weapon.weaponType.toString()) {
                var dpsFromMainAttribute = 1 + (this.primaryAttribute * 0.01);
                var dpsFromWeapon = this.weapon.getDamage() * this.weapon.getAttackSpeed();
                this.Dps = dpsFromWeapon * dpsFromMainAttribute;
                return true;
            }
        }
        throw new InvalidWeaponException("You cant use this type of weapon");
    }

    public void setUsableWeapons(ArrayList<String> usableWeapons) {
        this.usableWeapons = usableWeapons;
    }
    public void setUsableArmor(ArrayList<String> usableArmors) {
        this.usableArmors = usableArmors;
    }
    public void setWeapon(Weapon weapon) throws Exception {
        this.weapon = weapon;
        this.setDps();
    }
    public int getPrimaryAttribute() {
        return primaryAttribute;
    }
    public String getName() {
        return name;
    }
    public int getLevel() {
        return level;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }
    public void setTotalAttributes() throws Exception {
        for(Slot c : Slot.values()) {
            if(gear.get(c) != null){
                this.dexterityFromGear += gear.get(c).getPrimaryAttributes().getDexterity();
                this.intelligenceFromGear += gear.get(c).getPrimaryAttributes().getIntelligence();
                this.strengthFromGear += gear.get(c).getPrimaryAttributes().getStrength();
            }
        }
        this.totalAttributes = new PrimaryAttributes(this.primaryAttributes.getIntelligence() + this.intelligenceFromGear, this.primaryAttributes.getStrength() + this.strengthFromGear, this.primaryAttributes.getDexterity() + this.dexterityFromGear);
        if(this.mainAttribute.equals("INT")){
            this.primaryAttribute = this.totalAttributes.getIntelligence();
        }
        if(this.mainAttribute.equals("DEX")) {
            this.primaryAttribute = this.totalAttributes.getDexterity();
        }
        if(this.mainAttribute.equals("STR")) {
            this.primaryAttribute = this.totalAttributes.getStrength();
        }
        setDps();
    }
    public void levelUp() throws Exception {
        this.level++;
    };

    public String displayStats() {
        return "Character{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", Intellect=" + this.totalAttributes.getIntelligence() +
                ", Dexterity=" + this.totalAttributes.getDexterity() +
                ", Strength=" + this.totalAttributes.getStrength() +
                ", Dps=" + Dps +
                '}';
    }
}
