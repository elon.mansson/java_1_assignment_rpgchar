package models;

import utils.Slot;
import utils.WeaponType;

public class Weapon {
    String name;
    int level;
    Slot slot;
    WeaponType weaponType;
    int damage;
    double attackSpeed;
    public Weapon(){
        this.name = "UNARMED";
        this.level = 0;
        this.slot = Slot.WEAPON;
        this.weaponType = WeaponType.NULL;
        this.damage = 1;
        this.attackSpeed = 1.0;
    }

    public Weapon(String name, int level, Slot slot, WeaponType weaponType, int damage, double attackSpeed) {
        this.name = name;
        this.level = level;
        this.slot = slot;
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }

    public String getName() {
        return name;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    @Override
    public String toString() {
        return "Weapon{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", slot=" + slot +
                ", weaponType=" + weaponType +
                ", damage=" + damage +
                ", attackSpeed=" + attackSpeed +
                '}';
    }
}
