# 👏 Small Rpg game

This is a small Java application that was a small assignment to get a little familier with JAVA.

## ✌️ Description

### 🔥 About the application

The game is a RPG game where you can create a character (Mage, Warrior, Rogue or Ranger). Then you have the option to gear them with weapons and armors. 

The application contains the following functions:

* Create Character.

* Level up your character.

* Gear your character up with gear and weapons.

### 🤖 Technologies

* JAVA
* GIT
* JUNIT

## ⭐ Getting Started

### ⚠️ Dependencies

* We recommend to not change the versions in the dependencies. Otherwise, the application might not work. 

### ⚡ Installing

Follow the steps below to create and setup the translation application.

2. Clone the repo:
```
git clone https://gitlab.com/elon.mansson/java_1_assignment_rpgchar.git
``` 


### 💻 Executing program

* How to run the program:
```
Build the project with JDK 17.
```
```
Go into the test folder and run the tests!
```

## 😎 Authors

Contributors and contact information

Elon Månsson 
[@Elon.Mansson](https://gitlab.com/elon.mansson/)

## 🦝 Version History

* 0.1
    * Initial Release

## 🌌 License

This project is fully free for use.
