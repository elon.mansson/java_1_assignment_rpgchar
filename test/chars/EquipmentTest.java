package chars;

import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import models.Armor;
import models.PrimaryAttributes;
import models.Weapon;
import org.junit.jupiter.api.Test;
import utils.ArmorType;
import utils.Slot;
import utils.WeaponType;

import static org.junit.jupiter.api.Assertions.*;

class EquipmentTest {
    @Test
    public void warrior_checkIfExceptionIsThrownIfWeaponIsToHighLevel() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Weapon weapon = new Weapon("Test", 2, Slot.WEAPON, WeaponType.AXES, 2, 1.3);

        //ACT & ASSERT
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(weapon));
    }
    @Test
    public void warrior_checkIfExceptionIsThrownIfArmorIsToHighLevel() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Armor armor = new Armor("Test", 2, Slot.LEGS, ArmorType.PLATE, new PrimaryAttributes(0,0,0));

        //ACT & ASSERT
        assertThrows(InvalidArmorException.class, () -> warrior.setGear(armor));
    }
    @Test
    public void warrior_checkIfExceptionIsThrownIfWeaponIsWrongType() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Weapon weapon = new Weapon("Test", 1, Slot.WEAPON, WeaponType.BOWS, 2, 1.3);

        //ACT & ASSERT
        assertThrows(InvalidWeaponException.class, () -> warrior.setWeapon(weapon));
    }
    @Test
    public void warrior_checkIfExceptionIsThrownIfArmorIsWrongType() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Armor armor = new Armor("Test", 1, Slot.LEGS, ArmorType.CLOTH, new PrimaryAttributes(0,0,0));

        //ACT & ASSERT
        assertThrows(InvalidArmorException.class, () -> warrior.setGear(armor));
    }
    @Test
    public void warrior_checkIfWeaponIsValid_shouldReturnBoolean() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Weapon weapon = new Weapon("Test", 1, Slot.WEAPON, WeaponType.AXES, 2, 1.3);

        //ACT
        warrior.setWeapon(weapon);

        //ASSERT
        assertTrue(warrior.setDps());
    }
    @Test
    public void warrior_checkIfArmorIsValid_shouldReturnBoolean() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("Elon");
        Armor armor = new Armor("Test", 1, Slot.LEGS, ArmorType.PLATE, new PrimaryAttributes(0,0,0));

        //ACT & ASSERT
        assertTrue(warrior.setGear(armor));
    }
    @Test
    public void warrior_checkDpsWithOutWeapon_shouldReturnDps() throws Exception {
        //ARRANGE
        double expected = 1*(1 + (5 * 0.01));
        Warrior warrior = new Warrior("ELON");

        //ACT
        var actual = warrior.getDps();

        //ASSERT
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_checkDpsWithWeapon_shouldReturnDps() throws Exception {
        //ARRANGE
        double expected = (2 * 1.3)*(1 + (5 * 0.01));
        Warrior warrior = new Warrior("ELON");
        Weapon weapon = new Weapon("Test", 1, Slot.WEAPON, WeaponType.AXES, 2, 1.3);

        //ACT
        warrior.setWeapon(weapon);
        var actual = warrior.getDps();

        //ASSERT
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_checkDpsWithWeaponAndArmor_shouldReturnDps() throws Exception {
        //ARRANGE
        double expected = (7 * 1.1)*(1 + (8 * 0.01));
        Warrior warrior = new Warrior("ELON");
        Weapon weapon = new Weapon("Test", 1, Slot.WEAPON, WeaponType.AXES, 7, 1.1);
        Armor armor = new Armor("TEST", 1, Slot.HEAD, ArmorType.PLATE, new PrimaryAttributes(0,3,0));

        //ACT
        warrior.setWeapon(weapon);
        warrior.setGear(armor);
        var actual = warrior.getDps();

        //ASSERT
        assertEquals(expected, actual);
    }
}