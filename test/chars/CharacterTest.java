package chars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CharacterTest {
    @Test
    public void warrior_checkIfLevelIsOneOnCreation_shouldReturnOne() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("SEAN");
        var actual = warrior.getLevel();
        int expected = 1;

        //ACT & ASSERT
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_checkIfLevelUpWorks_shouldReturnTwo() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("SEAN");
        int expected = 2;

        //ACT
        warrior.levelUp();
        var actual = warrior.getLevel();

        //ASSERT
        assertEquals(expected, actual);
    }
    @Test
    public void warrior_checkIfBaseStatsAreCurrect() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("SEAN");
        int strExpected = 5;
        int dexExpected = 2;
        int intExpected = 1;

        //ACT
        var actualStr = warrior.getPrimaryAttributes().getStrength();
        var actualDex = warrior.getPrimaryAttributes().getDexterity();
        var actualInt = warrior.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void ranger_checkIfBaseStatsAreCurrect() throws Exception {
        //ARRANGE
        Ranger ranger = new Ranger("SEAN");
        int strExpected = 1;
        int dexExpected = 7;
        int intExpected = 1;

        //ACT
        var actualStr = ranger.getPrimaryAttributes().getStrength();
        var actualDex = ranger.getPrimaryAttributes().getDexterity();
        var actualInt = ranger.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void mage_checkIfBaseStatsAreCurrect() throws Exception {
        //ARRANGE
        Mage mage = new Mage("SEAN");
        int strExpected = 1;
        int dexExpected = 1;
        int intExpected = 8;

        //ACT
        var actualStr = mage.getPrimaryAttributes().getStrength();
        var actualDex = mage.getPrimaryAttributes().getDexterity();
        var actualInt = mage.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void rogue_checkIfBaseStatsAreCurrect() throws Exception {
        //ARRANGE
        Rogue rogue = new Rogue("SEAN");
        int strExpected = 2;
        int dexExpected = 6;
        int intExpected = 1;

        //ACT
        var actualStr = rogue.getPrimaryAttributes().getStrength();
        var actualDex = rogue.getPrimaryAttributes().getDexterity();
        var actualInt = rogue.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void rogue_checkIfBaseStatsAreCurrectOnLevelTwo() throws Exception {
        //ARRANGE
        Rogue rogue = new Rogue("SEAN");
        int strExpected = 3;
        int dexExpected = 10;
        int intExpected = 2;

        //ACT
        rogue.levelUp();
        var actualStr = rogue.getPrimaryAttributes().getStrength();
        var actualDex = rogue.getPrimaryAttributes().getDexterity();
        var actualInt = rogue.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void warrior_checkIfBaseStatsAreCurrectOnLevelTwo() throws Exception {
        //ARRANGE
        Warrior warrior = new Warrior("SEAN");
        int strExpected = 8;
        int dexExpected = 4;
        int intExpected = 2;

        //ACT
        warrior.levelUp();
        var actualStr = warrior.getPrimaryAttributes().getStrength();
        var actualDex = warrior.getPrimaryAttributes().getDexterity();
        var actualInt = warrior.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void mage_checkIfBaseStatsAreCurrectOnLevelTwo() throws Exception {
        //ARRANGE
        Mage mage = new Mage("SEAN");
        int strExpected = 2;
        int dexExpected = 2;
        int intExpected = 13;

        //ACT
        mage.levelUp();
        var actualStr = mage.getPrimaryAttributes().getStrength();
        var actualDex = mage.getPrimaryAttributes().getDexterity();
        var actualInt = mage.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
    @Test
    public void ranger_checkIfBaseStatsAreCurrectOnLevelTwo() throws Exception {
        //ARRANGE
        Ranger ranger = new Ranger("SEAN");
        int strExpected = 2;
        int dexExpected = 12;
        int intExpected = 2;

        //ACT
        ranger.levelUp();
        var actualStr = ranger.getPrimaryAttributes().getStrength();
        var actualDex = ranger.getPrimaryAttributes().getDexterity();
        var actualInt = ranger.getPrimaryAttributes().getIntelligence();

        //ASSERT
        assertEquals(strExpected, actualStr);
        assertEquals(dexExpected, actualDex);
        assertEquals(intExpected, actualInt);
    }
}
